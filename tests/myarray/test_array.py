from unittest import TestCase
from myarray.basearray import BaseArray
import myarray.basearray as ndarray
import io
import sys


class TestArray(TestCase):
    def test___init__(self):

        # simple init tests, other things are tester later
        try:
            a = BaseArray((2,))
            a = BaseArray([2, 5])
            a = BaseArray((2, 5), dtype=int)
            a = BaseArray((2, 5), dtype=float)
        except Exception as e:
            self.fail(f'basic constructor test failed, with exception {e:}')

        # test invalid parameters as shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray(5)
        self.assertEqual(('shape 5 is not a tuple or list',),
                         cm.exception.args)

        # test invalid value in shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray((4, 'a'))
        self.assertEqual(('shape (4, \'a\') contains a non integer a',),
                         cm.exception.args)

        # TODO test invalid dtype

        # test valid data param, list, tuple
        a = BaseArray((3, 4), dtype=int, data=list(range(12)))
        a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

        # test invalid data param
        # invalid data length
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 4), dtype=int, data=list(range(12)))
        self.assertEqual(('shape (2, 4) does not match provided data length 12',),
                         cm.exception.args)

        # inconsistent data type
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
        self.assertEqual(('provided data does not have a consistent type',),
                         cm.exception.args)

    def test_shape(self):
        a = BaseArray((5,))
        self.assertTupleEqual(a.shape, (5,))
        a = BaseArray((2, 5))
        self.assertTupleEqual(a.shape, (2, 5))
        a = BaseArray((3, 2, 5))
        self.assertEqual(a.shape, (3, 2, 5))

    def test_dtype(self):
        # test if setting a type returns the set type
        a = BaseArray((1,))
        self.assertIsInstance(a[1], float)
        a = BaseArray((1,), int)
        self.assertIsInstance(a[1], int)
        a = BaseArray((1,), float)
        self.assertIsInstance(a[1], float)

    def test_get_set_item(self):
        # test if setting a value returns the value
        a = BaseArray((1,))
        a[1] = 1
        self.assertEqual(1, a[1])
        # test for multiple dimensions
        a = BaseArray((2, 1))
        a[1, 1] = 1
        a[2, 1] = 1
        self.assertEqual(1, a[1, 1])
        self.assertEqual(1, a[2, 1])

        # test with invalid indices
        a = BaseArray((2, 2))
        with self.assertRaises(Exception) as cm:
            a[4, 1] = 0
        self.assertEqual(('indice (4, 1), axis 0 out of bounds [1, 2]',),
                         cm.exception.args)

        # test invalid type of ind
        with self.assertRaises(Exception) as cm:
            a[1, 1.1] = 0
        self.assertEqual(cm.exception.args,
                         ('(1, 1.1) is not a valid indice',))

        with self.assertRaises(Exception) as cm:
            a[1, 'a'] = 0
        self.assertEqual(('(1, \'a\') is not a valid indice',),
                         cm.exception.args)

        # test invalid number of ind
        with self.assertRaises(Exception) as cm:
            a[2] = 0
        self.assertEqual(('indice (2,) is not valid for this myarray',),
                         cm.exception.args)

        # test data intialized via data parameter
        a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
        self.assertEqual(3, a[1, 1, 4])
        self.assertEqual(13, a[2, 1, 2])

        # TODO enter invalid type

    def test_iter(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in a]
        self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

    def test_reversed(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in reversed(a)]
        self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

    def test_contains(self):
        a = BaseArray((1,), dtype=float)
        a[1] = 1

        self.assertIn(1., a)
        self.assertIn(1, a)
        self.assertNotIn(0, a)

    def test_shape_to_steps(self):
        shape = (4, 2, 3)
        steps_exp = (6, 3, 1)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = [1]
        steps_exp = (1,)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = []
        steps_exp = ()
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

    def test_multi_to_lin_ind(self):
        # shape = 4, 2,
        # 0 1
        # 2 3
        # 4 5
        # 6 7
        steps = 2, 1
        multi_lin_inds_pairs = (((1, 1), 0),
                                ((2, 2), 3),
                                ((4, 1), 6),
                                ((4, 2), 7))
        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_exp, lin_ind_res)

        # shape = 2, 3, 4
        #
        # 0 1 2 3
        # 4 5 6 7
        #
        #  8  9 10 11
        # 12 13 14 15
        steps = 8, 4, 1
        multi_lin_inds_pairs = (((1, 1, 1), 0),
                                ((2, 2, 1), 12),
                                ((1, 1, 4), 3),
                                ((2, 2, 3), 14))

        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_res, lin_ind_exp)

    def test_is_valid_instance(self):
        self.assertTrue(ndarray._is_valid_indice(2))
        self.assertTrue(ndarray._is_valid_indice((2, 1)))

        self.assertFalse(ndarray._is_valid_indice(()))
        self.assertFalse(ndarray._is_valid_indice([2]))
        self.assertFalse(ndarray._is_valid_indice(2.0))
        self.assertFalse(ndarray._is_valid_indice((2, 3.)))

    def test_multi_ind_iterator(self):
        shape = (30,)
        ind = (slice(10, 20, 3),)
        ind_list_exp = ((10,),
                        (13,),
                        (16,),
                        (19,))
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        shape = (5, 5, 5)
        ind = (2, 2, 2)
        ind_list_exp = ((2, 2, 2),)
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (2, slice(3), 2)
        ind_list_exp = ((2, 1, 2),
                        (2, 2, 2),
                        (2, 3, 2),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (slice(None, None, 2), slice(3), slice(2, 4))
        ind_list_exp = ((1, 1, 2), (1, 1, 3), (1, 1, 4),
                        (1, 2, 2), (1, 2, 3), (1, 2, 4),
                        (1, 3, 2), (1, 3, 3), (1, 3, 4),
                        (3, 1, 2), (3, 1, 3), (3, 1, 4),
                        (3, 2, 2), (3, 2, 3), (3, 2, 4),
                        (3, 3, 2), (3, 3, 3), (3, 3, 4),
                        (5, 1, 2), (5, 1, 3), (5, 1, 4),
                        (5, 2, 2), (5, 2, 3), (5, 2, 4),
                        (5, 3, 2), (5, 3, 3), (5, 3, 4),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

    def test_print_positive_int(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 5, 2, 4, 6, 2))
        o = '{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n'.format(1, 5, 2, 4, 6, 2)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        BaseArray.printar(a)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), o)

    def test_print_3d_int(self):
        a = BaseArray((2, 3, 2), dtype=int, data=(1, -5, 2, 4, -6, 2, 5, 6, 1, 9, 4, 10))
        o = 'layer: 0\n{:5d}{:5d}\n{:5d}{:5d}\n{:5d}{:5d}\nlayer: 1\n{:5d}{:5d}\n{:5d}{:5d}\n{:5d}{:5d}\n'.format(1, -5, 2, 4, -6, 2, 5, 6, 1, 9, 4, 10)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        BaseArray.printar(a)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), o)

    def test_print_3d_float(self):
        a = BaseArray((2, 3, 2), dtype=float, data=(1., -5.5, 2., 4., -6., 2.22, 5., 6., 1., 9., 4., 10.))
        o = 'layer: 0\n{:7.3f}{:7.3f}\n{:7.3f}{:7.3f}\n{:7.3f}{:7.3f}\nlayer: 1\n{:7.3f}{:7.3f}\n{:7.3f}{:7.3f}\n' \
            '{:7.3f}{:7.3f}\n'.format(1., -5.5, 2., 4., -6., 2.22, 5., 6., 1., 9., 4., 10.)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        BaseArray.printar(a)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), o)

    def test_print_negative_int(self):
        a = BaseArray((2, 3), dtype=int, data=(-1, 5, -2, -4, 6, 2))
        o = '{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n'.format(-1, 5, -2, -4, 6, 2)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        BaseArray.printar(a)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), o)

    def test_print_positive_float(self):
        a = BaseArray((2, 3), dtype=float, data=(1.5, 5., 2.8, 4., 6., 2))
        o = '{:7.3f}{:7.3f}{:7.3f}\n{:7.3f}{:7.3f}{:7.3f}\n'.format(1.5, 5., 2.8, 4., 6., 2)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        BaseArray.printar(a)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), o)

    def test_print_negative_float(self):
        a = BaseArray((2, 3), dtype=float, data=(-1.5, -5., -2.8, 4., 6., 2))
        o = '{:7.3f}{:7.3f}{:7.3f}\n{:7.3f}{:7.3f}{:7.3f}\n'.format(-1.5, -5., -2.8, 4., 6., 2)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        BaseArray.printar(a)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), o)

    def test_merge_sort_1d(self):
        d = BaseArray((3,), dtype=int, data=(2, -2, 1))
        ds = BaseArray((3,), dtype=int, data=(-2, 1, 2))
        BaseArray.merge_sort_ar(d, 1)
        self.assertListEqual([x for x in ds], [x for x in d])

    def test_merge_sort_2d_line(self):
        d = BaseArray((2, 3), dtype=int, data=(1, -5, 2, 4, -6, 2))
        ds = BaseArray((2, 3), dtype=int, data=(-5, 1, 2, -6, 2, 4))
        BaseArray.merge_sort_ar(d, 1)
        self.assertListEqual([x for x in ds], [x for x in d])

    def test_merge_sort_2d_collumn(self):
        d = BaseArray((2, 3), dtype=int, data=(1, -5, 2, 4, -6, 2))
        ds = BaseArray((2, 3), dtype=int, data=(1, -6, 2, 4, -5, 2))
        BaseArray.merge_sort_ar(d, 0)
        self.assertListEqual([x for x in ds], [x for x in d])

    def test_merge_sort_1d_float(self):
        d = BaseArray((3,), dtype=float, data=(2.5, -2.66, 1.))
        ds = BaseArray((3,), dtype=float, data=(-2.66, 1., 2.5))
        BaseArray.merge_sort_ar(d, 1)
        self.assertListEqual([x for x in ds], [x for x in d])

    def test_merge_sort_2d_line_float(self):
        d = BaseArray((2, 3), dtype=float, data=(1., -5.5, 2., 4., -6., 2.))
        ds = BaseArray((2, 3), dtype=float, data=(-5.5, 1., 2., -6., 2., 4.))
        BaseArray.merge_sort_ar(d, 1)
        self.assertListEqual([x for x in ds], [x for x in d])

    def test_merge_sort_2d_collumn_float(self):
        d = BaseArray((2, 3), dtype=float, data=(1., -5.5, 2., 4., -6.1, 2.))
        ds = BaseArray((2, 3), dtype=float, data=(1., -6.1, 2., 4., -5.5, 2.))
        BaseArray.merge_sort_ar(d, 0)
        self.assertListEqual([x for x in ds], [x for x in d])

    def test_find_in_ar(self):
        d = BaseArray((9,), dtype=int, data=(2, -2, 1, 2, 6, 2, 5, 2, 0))
        out = '((0), (3), (5), (7))'
        a = BaseArray.find_in_ar(d, 2)
        self.assertEqual(out, a)

    def test_find_in_ar_float(self):
        d = BaseArray((8,), dtype=float, data=(2., -2., 1.5, 2.44, 6., 2., 5., 2.))
        out = '((0), (5), (7))'
        a = BaseArray.find_in_ar(d, 2)
        self.assertEqual(a, out)

    def test_find_in_ar_2d(self):
        d = BaseArray((3, 3), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        out = '((1, 0), (2, 1), (2, 2))'
        a = BaseArray.find_in_ar(d, 2)
        self.assertEqual(a, out)

    def test_find_in_ar_2d_float(self):
        d = BaseArray((3, 3), dtype=float, data=(0., 0.34, 0., 2., 0.34, 0., 0., 2., 2.))
        out = '((1, 0), (2, 1), (2, 2))'
        a = BaseArray.find_in_ar(d,2)
        self.assertEqual(a, out)

    def test_find_in_ar_3d(self):
        d = BaseArray((2, 3, 2), dtype=int, data=(1, -5, 2, 4, -6, 2, 5, 6, 1, 9, 4, 10))
        out = '((0, 1, 0), (0, 2, 1))'
        a = BaseArray.find_in_ar(d, 2)
        self.assertEqual(a, out)

    def test_find_in_ar_3d_float(self):
        d = BaseArray((2, 3, 2), dtype=float, data=(1.54, -5., 2., 4.5, -6.4, 2., 5.55, 6., 1., 9., 4., 10.))
        out = '((0, 1, 0), (0, 2, 1))'
        a = BaseArray.find_in_ar(d, 2)
        self.assertEqual(a, out)

    def test_sum_ar_arar(self):
        d = BaseArray((2, 3), dtype=int, data=(0, 0, 0, 0, 0, 0))
        d1 = BaseArray((2, 3), dtype=int, data=(1, 1, 1, 1, 1, 1))
        rez = BaseArray((2, 3), dtype=int, data=(1, 1, 1, 1, 1, 1))
        d = BaseArray.sum_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_sum_ar_arint(self):
        d = BaseArray((2, 3), dtype=int, data=(0, 0, 0, 0, 0, 0))
        d1 = 1
        rez = BaseArray((2, 3), dtype=int, data=(1, 1, 1, 1, 1, 1))
        d = BaseArray.sum_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_sum_ar_arar_float(self):
        d = BaseArray((2, 3), dtype=float, data=(0., 0., 0., 0., 0., 0.))
        d1 = BaseArray((2, 3), dtype=float, data=(1.1, 1.1, 1.1, 1.1, 1.1, 12.))
        rez = BaseArray((2, 3), dtype=float, data=(1.1, 1.1, 1.1, 1.1, 1.1, 12.))
        d = BaseArray.sum_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_dif_ar_arar(self):
        d = BaseArray((2, 3), dtype=int, data=(0, 0, 0, 0, 0, 0))
        d1 = BaseArray((2, 3), dtype=int, data=(1, 1, 1, 1, 1, 1))
        rez = BaseArray((2, 3), dtype=int, data=(-1, -1, -1, -1, -1, -1))
        d = BaseArray.dif_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_dif_ar_arint(self):
        d = BaseArray((2, 3), dtype=int, data=(0, 0, 0, 0, 0, 0))
        d1 = 1
        rez = BaseArray((2, 3), dtype=int, data=(-1, -1, -1, -1, -1, -1))
        d = BaseArray.dif_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_dif_ar_arar_float(self):
        d = BaseArray((2, 3), dtype=float, data=(0., 0., 0., 0., 0., 0.))
        d1 = BaseArray((2, 3), dtype=float, data=(1.1, 1.1, 1.1, 1.1, 1.1, 12.))
        rez = BaseArray((2, 3), dtype=float, data=(-1.1, -1.1, -1.1, -1.1, -1.1, -12.))
        d = BaseArray.dif_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_div_ar_arar(self):
        d = BaseArray((2, 3), dtype=int, data=(4, 4, 4, 4, 4, 4))
        d1 = BaseArray((2, 3), dtype=int, data=(2, 2, 2, 2, 2, 2))
        rez = BaseArray((2, 3), dtype=float, data=(2., 2., 2., 2., 2., 2.))
        d = BaseArray.div_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_div_ar_arfloat(self):
        d = BaseArray((2, 3), dtype=int, data=(4, 4, 4, 4, 4, 4))
        d1 = 2.0
        rez = BaseArray((2, 3), dtype=float, data=(2., 2., 2., 2., 2., 2.))
        d = BaseArray.div_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_mult_ar_arar(self):
        d = BaseArray((2, 3), dtype=int, data=(4, 4, 4, 4, 4, 4))
        d1 = BaseArray((2, 3), dtype=int, data=(2, 2, 2, 2, 2, 2))
        rez = BaseArray((2, 3), dtype=float, data=(8, 8, 8, 8, 8, 8))
        d = BaseArray.mult_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_mult_ar_arfloat(self):
        d = BaseArray((2, 3), dtype=int, data=(4, 4, 4, 4, 4, 4))
        d1 = 2.0
        rez = BaseArray((2, 3), dtype=float, data=(8., 8., 8., 8., 8., 8.))
        d = BaseArray.mult_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_log_ar_arar(self):
        d = BaseArray((2, 3), dtype=int, data=(4, 4, 4, 4, 4, 4))
        d1 = BaseArray((2, 3), dtype=int, data=(2, 2, 2, 2, 2, 2))
        rez = BaseArray((2, 3), dtype=float, data=(2, 2, 2, 2, 2, 2))
        d = BaseArray.log_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_log_ar_arfloat(self):
        d = BaseArray((2, 3), dtype=int, data=(4, 4, 4, 4, 4, 4))
        d1 = 2.0
        rez = BaseArray((2, 3), dtype=float, data=(2., 2., 2., 2., 2., 2.))
        d = BaseArray.log_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_pow_ar_arar(self):
        d = BaseArray((2, 3), dtype=int, data=(4, 4, 4, 4, 4, 4))
        d1 = BaseArray((2, 3), dtype=int, data=(2, 2, 2, 2, 2, 2))
        rez = BaseArray((2, 3), dtype=float, data=(16, 16, 16, 16, 16, 16))
        d = BaseArray.pow_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_pow_ar_arfloat(self):
        d = BaseArray((2, 3), dtype=int, data=(4, 4, 4, 4, 4, 4))
        d1 = 2.0
        rez = BaseArray((2, 3), dtype=float, data=(16., 16., 16., 16., 16., 16.))
        d = BaseArray.pow_ar(d, d1)
        self.assertListEqual([x for x in d], [x for x in rez])

    def test_matix_mult(self):
        a = BaseArray((3, 3), dtype=int, data=(12, 7, 3, 4, 5, 6, 7, 8, 9))
        b = BaseArray((3, 4), dtype=int, data=(5, 8, 1, 2, 6, 7, 3, 0, 4, 5, 9, 1))
        c = BaseArray((3, 4), dtype=int, data=(114, 160, 60, 27, 74, 97, 73, 14, 119, 157, 112, 23))
        rez = BaseArray.matrix_mult(a, b)
        self.assertListEqual([x for x in rez], [x for x in c])





